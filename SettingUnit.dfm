object SettingForm: TSettingForm
  Left = 192
  Top = 114
  BorderStyle = bsDialog
  Caption = #30701#20449#35774#32622
  ClientHeight = 122
  ClientWidth = 266
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 24
    Top = 24
    Width = 36
    Height = 12
    Caption = #20018#21475#65306
  end
  object Label2: TLabel
    Left = 24
    Top = 56
    Width = 36
    Height = 12
    Caption = #36895#29575#65306
  end
  object Label3: TLabel
    Left = 24
    Top = 88
    Width = 60
    Height = 12
    Caption = #21457#36865#36895#24230#65306
  end
  object Label4: TLabel
    Left = 136
    Top = 88
    Width = 102
    Height = 12
    Caption = #27599#38548'n'#31186#21457#19968#26465#30701#20449
  end
  object ComPort: TComboBox
    Left = 64
    Top = 16
    Width = 65
    Height = 20
    ItemHeight = 12
    ItemIndex = 0
    TabOrder = 0
    Text = '1'
    Items.Strings = (
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9'
      '10'
      '11'
      '12'
      '13'
      '14'
      '15'
      '16'
      '17'
      '18'
      '19'
      '20')
  end
  object ComBaud: TComboBox
    Left = 64
    Top = 48
    Width = 65
    Height = 20
    ItemHeight = 12
    ItemIndex = 2
    TabOrder = 1
    Text = '9600'
    Items.Strings = (
      '2400'
      '4800'
      '9600'
      '19200'
      '38400'
      '57600'
      '115200')
  end
  object BitBtn1: TBitBtn
    Left = 144
    Top = 16
    Width = 75
    Height = 25
    TabOrder = 2
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 144
    Top = 48
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333333333000033338833333333333333333F333333333333
      0000333911833333983333333388F333333F3333000033391118333911833333
      38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
      911118111118333338F3338F833338F3000033333911111111833333338F3338
      3333F8330000333333911111183333333338F333333F83330000333333311111
      8333333333338F3333383333000033333339111183333333333338F333833333
      00003333339111118333333333333833338F3333000033333911181118333333
      33338333338F333300003333911183911183333333383338F338F33300003333
      9118333911183333338F33838F338F33000033333913333391113333338FF833
      38F338F300003333333333333919333333388333338FFF830000333333333333
      3333333333333333333888330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object edtSpeed: TEdit
    Left = 88
    Top = 80
    Width = 41
    Height = 20
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    TabOrder = 4
    Text = '5'
  end
end
