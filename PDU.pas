unit PDU;

interface

uses
  Classes,Contnrs,Dialogs,SysUtils,StrUtils;

  function DecodeEnglish(s: string): string;//纯英文解码
  function EncodeChinese(s:WideString):String;//中文格式编码,s为Unicode String
  function DecodeChinese(InputStr:String):String;//中文格式解码

implementation

function DecodeEnglish(s: string): string;
  function HexCharToInt(HexToken: char): Integer;
  begin
    {if HexToken>#97 then HexToken:=Chr(Ord(HexToken)-32);
    { use lowercase aswell }

    Result := 0;

    if (HexToken > #47) and (HexToken < #58) then { chars 0....9 }
      Result := Ord(HexToken) - 48
    else
      if (HexToken > #64) and (HexToken < #71) then { chars A....F }
        Result := Ord(HexToken) - 65 + 10;
  end;

  function HexCharToBin(HexToken: char): string;
  var
    DivLeft: integer;
  begin
    DivLeft := HexCharToInt(HexToken); { first HEX->BIN }
    Result := '';
    { Use reverse dividing }
    repeat { Trick; divide by 2 }
      if odd(DivLeft) then { result = odd ? then bit = 1 }
        Result := '1' + Result { result = even ? then bit = 0 }
      else
        Result := '0' + Result;

      DivLeft := DivLeft div 2; { keep dividing till 0 left and length = 4 }
    until (DivLeft = 0) and (length(Result) = 4); { 1 token = nibble = 4 bits }
  end;

  function HexToBin(HexNr: string): string;
    { only stringsize is limit of binnr }
  var
    Counter: integer;
  begin
    Result := '';

    for Counter := 1 to length(HexNr) do
      Result := Result + HexCharToBin(HexNr[Counter]);
  end;

  function pow(base, power: integer): integer;
  var
    counter: integer;
  begin
    Result := 1;

    for counter := 1 to power do
      Result := Result * base;
  end;

  function BinStrToInt(BinStr: string): integer;
  var
    counter: integer;
  begin
    if length(BinStr) > 16 then
      raise ERangeError.Create(#13 + BinStr + #13 +
        'is not within the valid range of a 16 bit binary.' + #13);

    Result := 0;

    for counter := 1 to length(BinStr) do
      if BinStr[Counter] = '1' then
        Result := Result + pow(2, length(BinStr) - counter);
  end;

var
  OctetStr: string;
  OctetBin: string;
  Charbin: string;
  PrevOctet: string;
  Counter: integer;
  Counter2: integer;
begin
  PrevOctet := '';
  Result := '';

  for Counter := 1 to length(s) do
  begin
    if length(PrevOctet) >= 7 then { if 7 Bit overflow on previous }
    begin
      if BinStrToInt(PrevOctet) <> 0 then
        Result := Result + Chr(BinStrToInt(PrevOctet))
      else
        Result := Result + ' ';

      PrevOctet := '';
    end;

    if Odd(Counter) then { only take two nibbles at a time }
    begin
      OctetStr := Copy(s, Counter, 2);
      OctetBin := HexToBin(OctetStr);

      Charbin := '';
      for Counter2 := 1 to length(PrevOctet) do
        Charbin := Charbin + PrevOctet[Counter2];

      for Counter2 := 1 to 7 - length(PrevOctet) do
        Charbin := OctetBin[8 - Counter2 + 1] + Charbin;

      if BinStrToInt(Charbin) <> 0 then
        Result := Result + Chr(BinStrToInt(CharBin))
      else
        Result := Result + ' ';

      PrevOctet := Copy(OctetBin, 1, length(PrevOctet) + 1);
    end;
  end;
end;

function EncodeChinese(s:WideString):String;//中文格式编码,s为Unicode String
var
  i,len:Integer;
  cur:Integer;
  t:String;
begin
  Result:='';
  len:=Length(s);
  i:=1;
  while i<=len do
  begin
    cur:=ord(s[i]); //先返回序数值
    //BCD转换
    FmtStr(t,'%4.4X',[cur]); //格式化序数值（BCD转换）
    Result:=Result+t;
    inc(i);
   end;
end;

function DecodeChinese(InputStr:String):String;//中文格式解码
var
  Buf:array[0..300] of WideChar;
  i,j:Integer;
begin
  i:=0;
  while i<Length(InputStr) do
  begin
    j:=StrToInt('0x'+Copy(InputStr,i+1,4));
    Buf[i div 4]:=WideChar(j);
    i:=i+4;
  end;
  Buf[i div 4]:=#0;
  Result:=WideCharToString(Buf);
end;

end.
