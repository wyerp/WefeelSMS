unit SMSExports;

interface
  procedure SMSCreate();stdcall;
  procedure SMSSetting(ComNumber:integer;Baud:integer);stdcall;
  procedure SMSStart();stdcall;
  procedure SMSStop();stdcall;
  procedure SMSFree();stdcall;
  procedure SMSPutSMS(pCode,pContent:PChar;id:integer);stdcall;
  function SMSGetSMS(pCode,pTime,pContent:PChar):boolean;stdcall;
  function SMSGetLine(pLine:PChar):boolean;stdcall;
  function SMSQueueLength():integer;stdcall;
  procedure SMSSettingWindow();stdcall;
  procedure SMSSetSendSpeed(Speed:integer);stdcall;
  function SMSGetSendSpeed():integer;stdcall;
  function SMSGetState(out id,state:integer):boolean;stdcall;
implementation

uses SMS, SettingUnit,SysUtils,Controls,Dialogs;
var
  sms:TShortMessage;

procedure SMSCreate();stdcall;
begin
  sms:=TShortMessage.Create;
end;
procedure SMSSetting(ComNumber:integer;Baud:integer);stdcall;
begin
  sms.ComNumber:=ComNumber;
  sms.Baud:=Baud;
end;
procedure SMSStart();stdcall;
begin
  if Assigned(sms) then sms.Open;
end;
procedure SMSStop();stdcall;
begin
  if Assigned(sms) then sms.Close;
end;
procedure SMSFree();stdcall;
begin
  if Assigned(sms) then sms.Free;
  sms:=nil;
end;
function SMSGetState(out id,state:integer):boolean;stdcall;
begin
  Result:=sms.GetFromResultSpool(id,state);
end;
procedure SMSPutSMS(pCode,pContent:PChar;id:integer);stdcall;
begin
  sms.AddSMSToSendSpool(pCode,pContent,id);
end;
function SMSGetSMS(pCode,pTime,pContent:PChar):boolean;stdcall;
begin
  Result:=sms.GetSMSFromRecvSpool(pCode,pTime,pContent);
end;
function SMSGetLine(pLine:PChar):boolean;stdcall;
begin
  Result:=sms.GetFromRecvSpool(pLine);
end;
function SMSQueueLength():integer;stdcall;
begin
  Result:=sms.WaitingSendCount
end;

procedure SMSSettingWindow();stdcall;
var
  SettingForm: TSettingForm;
begin
  if sms=nil then
  begin
    ShowMessage('短信接口未初始化');
    Exit;
  end;
  
  SettingForm:=TSettingForm.Create(nil);
  SettingForm.ComPort.Text:=IntToStr(sms.ComNumber);
  SettingForm.ComBaud.Text:=IntToStr(sms.Baud);
  SettingForm.edtSpeed.Text:=IntToStr(sms.SendSpeed);
  if SettingForm.ShowModal()=mrOK then
  begin
    sms.Close;
    try
      sms.ComNumber:=StrToInt(SettingForm.ComPort.Text);
      sms.Baud:=StrToInt(SettingForm.ComBaud.Text);
      sms.SendSpeed:=StrToInt(SettingForm.edtSpeed.Text);
      sms.Open;
    except
      ShowMessage('初始化错误！');
    end;
  end;
  SettingForm.Free;
end;

procedure SMSSetSendSpeed(Speed:integer);stdcall;
begin
  sms.SendSpeed:=Speed;
end;

function SMSGetSendSpeed():integer;stdcall;
begin
  Result:=sms.SendSpeed;
end;

end.
